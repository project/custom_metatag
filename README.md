# Custom Metatag

The Custom Metatag module allows you to add custom meta tags for any URL in the system, enhancing the SEO capabilities of your Drupal website.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/custom_metatag).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/custom_metatag).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at `Administration > Extend`.
2. Configure the module by navigating to `views.view.custom_metatag`.


## Maintainers

- Saurabh Kanva - [saurabhkanva](https://www.drupal.org/u/saurabhkanva)
