<?php

namespace Drupal\custom_metatag\Form;

use Drupal\Core\Database\Driver\corefake\Connection;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Form controller for the custom metatag entity edit forms.
 */
class CustomMetatagForm extends ContentEntityForm {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entityTypeManager) {
    $this->database = $database;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $label = $form_state->getValue('label')[0]['value'];
    $schema = $form_state->getValue('schema')[0]['value'];
    if (!empty($schema)) {
      $result = json_decode($schema);
      if ($result === NULL) {
        $error['schema'] = 'Schema sould be in JSON format';
        $error === TRUE ? TRUE : $form_state->setErrorByName('schema', $error['schema']);
      }
    }
    // Validation for route.
    if (!empty($label)) {
      if (substr($label, 0, 1) !== "/") {
        $error['label'] = 'The Route should start with /';
        $error === TRUE ? TRUE : $form_state->setErrorByName('label', $error['label']);
      }
    }
    $query = $this->entityTypeManager->getStorage('custom_metatag')
      ->getQuery()
      ->condition('label', $label)
      ->accessCheck(TRUE);
    $results = $query->execute();
    $entityId = key($results);
    if ($form_state->getformObject()->getEntity()->id() !== NULL) {
      if ($results && ("$entityId" !== $form_state->getformObject()->getEntity()->id())) {
        $error['label'] = 'The meta tags for the route are already added';
        $error === TRUE ? TRUE : $form_state->setErrorByName('label', $error['label']);
      }
    }
    else {
      if ($results) {
        $error['label'] = 'The meta tags for the route are already added';
        $error === TRUE ? TRUE : $form_state->setErrorByName('label', $error['label']);
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $robots = $form_state->getValue('meta_robots');
    if (!empty($robots)) {
      $robotString = implode(', ', array_keys($robots));
    }
    unset($form['meta_robots']);
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();
    $query = $this->database->update('custom_metatag');
    $query->fields([
      'robots' => $robotString,
    ]);
    $query->condition('id', $entity->id());
    $query->execute();
    $message_arguments = ['%label' => $entity->get('label')->getValue()[0]['value']];
    $logger_arguments = [
      '%label' => $entity->get('label')->getValue()[0]['value'],
      'label' => $entity->get('label')->getValue()[0]['value'],
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New custom metatags %label has been created.', $message_arguments));
        $this->logger('custom_metatag')->notice('Created new custom metatags %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The custom metatags %label has been updated.', $message_arguments));
        $this->logger('custom_metatag')->notice('Updated custom metatags %label.', $logger_arguments);
        break;
    }
    $url = Url::fromRoute('view.custom_metatag.page_1');
    $response = new RedirectResponse($url->toString());
    $response->send();

    return $result;
  }

}
