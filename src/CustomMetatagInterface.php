<?php

namespace Drupal\custom_metatag;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a custom metatag entity type.
 */
interface CustomMetatagInterface extends ContentEntityInterface, EntityChangedInterface {

}
